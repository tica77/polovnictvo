//
//  PhotoCollectionCell.h
//  apka
//
//  Created by Luky on 03/12/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku FOTKY


#import <UIKit/UIKit.h>

@interface PhotoCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;
@property (weak, nonatomic) IBOutlet UIImageView *parseImage;

@end
