//
//  PolovnictvoVC.h
//  apka
//
//  Created by Martin on 31.10.2013.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku DOMOV


#import <UIKit/UIKit.h>

@interface PolovnictvoVC : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;

@end
