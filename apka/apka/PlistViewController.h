//
//  PlistTutorialViewController.h
//  PlistTutorial
//
//  Created by Nick Barrowclough on 6/5/13.
//  Copyright (c) 2013 iSoftware Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlistTutorialViewController : UIViewController {
    NSMutableArray *mountainsArray;
}
- (IBAction)logDictionary:(id)sender;

- (IBAction)editDictionary:(id)sender;

@end
