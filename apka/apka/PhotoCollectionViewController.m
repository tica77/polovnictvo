//
//  photoCollectionViewController.m
//  apka
//
//  Created by Luky on 02/12/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import "PhotoCollectionViewController.h"

@interface PhotoCollectionViewController (){
 
    NSString *finalPath;
    NSArray* sortedArray;
    BOOL local;

}

@end

@implementation PhotoCollectionViewController

@synthesize collectionViewScreen;
@synthesize photoSegue;
@synthesize imagePath;
@synthesize tempPhotoArray;
@synthesize PhotoArraySegue;
@synthesize currrentIndex;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    tempPhotoArray = [[NSMutableArray alloc] init];
    
    // zisti, ci zvolena polozka sa rovna jednemu z ulozenych fotoalbumov v aplikacii
    if (([photoSegue  isEqual: @"posedy"]) ||
        ([photoSegue  isEqual: @"fotopasca"]) ||
        ([photoSegue  isEqual: @"strelba"]) ||
        ([photoSegue  isEqual: @"zlatyJelen"]) ||
        ([photoSegue  isEqual: @"trofeje"]))   {
        // zavola nacitanie obrazkov z pameti aplikacie
        [self queryOffLine];
    }
    else {
        // ak sa nenachadza album v pameti, dojde k nacitaniu z cloudovej sluzby Parse
        [self queryParseMethod];
    }
}
- (void) queryOffLine{
    // postupne nacita desat obrazkov, ktorych nazov je zlozeny z nazvu albumu a cisla napr. strelba_1.jpg
    for (NSInteger number = 1; number < 11; number++) {
        NSString *imageOffLine = [NSString stringWithFormat:@"%@_%d.jpg", photoSegue, number];
        // s tychto fotografii vytvori pole tempPhotoArray, ktore sa neskor pouzije na zobrazenie nahladov v collectionView
        [tempPhotoArray addObject:imageOffLine];
    }
    // pre jednoduchsie nacitanie obrazkov, ktore su ulozene lokalne
    local = YES;
}

-(void) queryParseMethod{
    // ak nie je album ulozeny na disku, nasleduje nacitanie fotiek z cloudovej sluzby Parse,
    local = NO;
    PFQuery *query = [PFQuery queryWithClassName: photoSegue];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        // ak nedojde k chybe pri nacitani obsahu triedy z Parse
        if (!error) {
            // vytvorime pole imagesFileArray kde sa ulozi do aplikacie cela trieda z parse
            imagesFileArray = [[NSArray alloc]initWithArray:objects];
            
            for (NSInteger number=0; number < 10; number++) {
                // postupne z imageFielArray sa vyberu len nazvy suborov obrazkov, ktore sa ulozia do tempPhototArray
                PFObject *tempObject = [imagesFileArray objectAtIndex:number];
                NSString *imageParse = [tempObject objectForKey: @"imageName"];
                NSString *imageParseName = [NSString stringWithFormat:@"%@.jpg", imageParse];
                [tempPhotoArray addObject:imageParseName];
            }
        }
        else {
        }
            
        [collectionViewScreen reloadData];
    }];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section{
    

    return [tempPhotoArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // skrijeme tabBar pre vacsiu plochu zobrazenia zariadenia
    self.tabBarController.tabBar.hidden = YES;
    
    
    static NSString *cellIdentifier = @"imageCell";
    PhotoCollectionCell *cell = (PhotoCollectionCell *)[collectionViewScreen dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    // ak su obrazky lokalne jednoducho nacita nahlady obrazkov a zabrazi ich
    if (local) {
        NSString *tempFile = [tempPhotoArray objectAtIndex:indexPath.row];
        cell.parseImage.image = [UIImage imageNamed:tempFile];
    }
    // ak sa nachadzaju na cloudovej sluzbe Parse, dojde podla nazvu subory k stiahnutiu dat obrazku
    else {
    PFObject *imageObject = [imagesFileArray objectAtIndex:indexPath.row];
    PFFile *imageFile = [imageObject objectForKey:@"imageFile"];
    NSString *imageName = [imageObject objectForKey:@"imageName"];
    
    [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        // ak nedojde k chybe, stiahnute data sa ulozia do aplikacie
        if (!error) {
          
            // zobrazi sa loadingSpinner
            [cell.loadingSpinner startAnimating];
            cell.loadingSpinner.hidden = NO;
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentDirectory = [paths objectAtIndex:0];
            NSString *imageNameJpg = [NSString stringWithFormat:@"%@.jpg", imageName];
            finalPath = [documentDirectory stringByAppendingPathComponent:imageNameJpg];
            // data sa ulozia do suboru s cestou finalPath
            if(data){
                [data writeToFile:finalPath atomically:YES];
            }

            cell.parseImage.image = [UIImage imageWithData:data ];
            // po nacitani obrazku dojde k vypnutiu loadingSpinner
            [cell.loadingSpinner stopAnimating];
            cell.loadingSpinner.hidden = YES;
        }
    }];
    }
    return cell;
    
    self.tabBarController.tabBar.hidden = YES;

}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // po kliknuti na jednotlive riadky zoznamu dojde k prechodu showPhoto na novy obrazovku
    if ([segue.identifier isEqualToString: @"showPhoto"]) {
        
        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] objectAtIndex:0];
        PhotoCollectionViewController *destViewController = segue.destinationViewController;
        
        // tento prechod nesie informcie o zvolenom albume a riadku, na ktory sa kliklo
        destViewController.PhotoArraySegue = tempPhotoArray;
        destViewController.currrentIndex = indexPath.row;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
