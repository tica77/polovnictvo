//
//  photoTableViewController.h
//  apka
//
//  Created by Luky on 02/12/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku FOTKY


#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface PhotoTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate> {
}

@property (strong, nonatomic) IBOutlet UITableView *photoTableView;

@property (strong, nonatomic) NSMutableArray *photoArray;
@property (nonatomic, strong) NSString *photoSegue;

@end
