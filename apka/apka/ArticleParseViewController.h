//
//  ArticleParseViewController.h
//  apka
//
//  Created by Luky on 23/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku CLANKY

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface ArticleParseViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource> {
   
}

@property (strong, nonatomic) IBOutlet UITableView *articleTable;

@property (nonatomic, strong) NSMutableArray *articlesArray;
@property (nonatomic, strong) NSString *articlePDF;
@property (nonatomic, strong) NSURL *articleURL;

@end
