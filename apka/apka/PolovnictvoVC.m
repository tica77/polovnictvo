//
//  PolovnictvoVC.m
//  apka
//
//  Created by Martin on 31.10.2013.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import "PolovnictvoVC.h"


@interface PolovnictvoVC ()

@end


@implementation PolovnictvoVC

@synthesize webView, loadingSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
    
}

//pri zacinani nacitavania stranky dojde k zobrazeniu loadingSpinner
- (void) webViewDidStartLoad:(UIWebView *)webView {
    loadingSpinner.hidden = NO;
    [loadingSpinner startAnimating];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // nastavenie cesty v aplikacii k ulozenej webovej stranke Vitajte!.html
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Vitajte!" ofType:@"html" inDirectory:@"html2"]];
   
    //zobrazenie stranky z ulozenej cesty, co odstrani loadingSpinner z obrazovky
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
    [loadingSpinner stopAnimating];
    loadingSpinner.hidden = YES;
    
}

- (void) webViewDidFinishLoad:(UIWebView *)webView  {
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
