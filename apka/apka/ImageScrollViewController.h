//
//  ImageScrollViewController.h
//  ImageScroll
//
//  Created by Luky on 02/12/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
//pre zalozku FOTKY

#import <UIKit/UIKit.h>

@interface ImageScrollViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) NSString *imagePath;
@property (strong, nonatomic) NSMutableArray *PhotoArraySegue;
@property (nonatomic, assign) NSInteger currrentIndex;

@end
