//
//  Controller.h
//  apka
//
//  Created by Luky on 13/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Controller : NSObject {
    NSString *personName;
    NSMutableArray *phoneNumbers;
}

@property (copy, nonatomic) NSString *personName;
@property (retain, nonatomic) NSMutableArray *phoneNumbers;

@end



