//
//  photoCollectionViewController.h
//  apka
//
//  Created by Luky on 02/12/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku FOTKY


#import <UIKit/UIKit.h>
#import "PhotoCollectionCell.h"
#import <Parse/Parse.h>


@interface PhotoCollectionViewController : UICollectionViewController <UICollectionViewDelegate, UICollectionViewDataSource>
{
    NSArray *imagesFileArray;
    NSMutableArray *imagesArray;
    
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewScreen;

@property (nonatomic, strong) NSString *photoSegue;
@property (strong, nonatomic) NSMutableArray *PhotoArraySegue;
@property (nonatomic, assign) NSInteger currrentIndex;
@property (strong, nonatomic) NSString *imagePath;
@property (strong, nonatomic) NSMutableArray *tempPhotoArray;



@end
