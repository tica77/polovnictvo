//
//  CellTableViewController.m
//  apka
//
//  Created by Luky on 31/10/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import "CellTableViewController.h"
#import "AnimalTypeViewController.h"
#import "AnimalDetailViewController.h"
#import "SWRevealViewController.h" 


@interface CellTableViewController ()

@end

@implementation CellTableViewController

@synthesize tempArray;

- (void)viewDidLoad
{
    // nastavenie side bar akcie. po stlaceni dojde k ukazke sidebaru
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    // nastavenie gesta pre posun do sidebaru
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

    /// nastavenie prechodu pre zobrazenie informacii o danom druhu zveri
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString: @"ShowAnimalData"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        AnimalDetailViewController *destViewController = segue.destinationViewController;
        destViewController.AnimalName = [[tempArray objectAtIndex:indexPath.row] objectForKey:@"name"];
        destViewController.animalDate = [[tempArray objectAtIndex:indexPath.row] objectForKey:@"odstrel"];
        destViewController.AnimalAbout  = [[tempArray objectAtIndex:indexPath.row] objectForKey:@"about"];
        destViewController.AnimalURL  = [[tempArray objectAtIndex:indexPath.row] objectForKey:@"URL"];
        destViewController.animalSrtstnata  = [[tempArray objectAtIndex:indexPath.row] objectForKey:@"srstnata"];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tempArray count ];
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    //nastavenie pozadia obrazovky na obrazok foto.jpg
    UIImage *foto = [UIImage imageNamed:@"foto.jpg" ];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:foto];
    imageView.contentMode = UIViewContentModeTopLeft;
    [tableView setBackgroundView:nil];
    tableView.backgroundView = imageView;
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    // zoznam je zlozeny z nazvou druhov zveri
    cell.textLabel.text  = [[tempArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    return cell;
}
@end
