//
//  ArticleParseViewController.m
//  apka
//
//  Created by Luky on 23/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import "ArticleParseViewController.h"
#import "Reachability.h"


@interface ArticleParseViewController (){
    Reachability *internetReachableFoo;
}

@end


@implementation ArticleParseViewController
@synthesize articlePDF;
@synthesize articleTable;
@synthesize articleURL;
@synthesize articlesArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //ziska cestu k suboru article.plist
    NSString *plistPath;
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:@"article.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle] pathForResource:@"article" ofType:@"plist"];
    }
    //ulozi udaje z article.plist do pola articleArray
    articlesArray = [[[NSMutableArray alloc] initWithContentsOfFile:plistPath]mutableCopy];
    [articleTable reloadData];
    
        // kontrola ci je dostupne internetove pripojenie
        internetReachableFoo = [Reachability reachabilityWithHostname:@"www.google.com"];
        
        // ak je internet dostupny...
         __weak typeof(self) weakSelf = self;
        internetReachableFoo.reachableBlock = ^(Reachability*reach)
        {
            // ...potom aktualizuj zoznam clankov z triedy Art z Parse.com
            dispatch_async(dispatch_get_main_queue(), ^{
                PFQuery *retrieveArticles = [PFQuery queryWithClassName:@"art"];
                [retrieveArticles findObjectsInBackgroundWithBlock:^(NSArray *object, NSError *error) {
                [weakSelf.articlesArray addObjectsFromArray:object];
                [weakSelf.articleTable reloadData];
                }];
            });
        };
    
        [internetReachableFoo startNotifier];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     return articlesArray.count;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString: @"showArticlePDF"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ArticleParseViewController *destViewController = segue.destinationViewController;
        
        // ziska cestu k sunboru PDF daneho clanku
        articlePDF  = [[articlesArray objectAtIndex:indexPath.row] objectForKey:@"PDFName"];        
        NSString *exists = [[NSBundle mainBundle] pathForResource:articlePDF ofType:@"pdf"];
        
        // ak nie je ulozeny clanok v aplikacii
        if(exists == nil){
            
            //nacita data clanku z parse.com
            PFObject *pdfObject =  [articlesArray objectAtIndex:indexPath.row];
            NSString *pdfName = [pdfObject objectForKey:@"PDFName"];
            PFFile *PDFile =  [pdfObject objectForKey:@"PDFFile"];
            NSData *resumeData = [PDFile getData];
            
            NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentDirectory=[paths objectAtIndex:0];
            NSString *finalPath=[documentDirectory stringByAppendingPathComponent:pdfName];
            
            // vytvori lokalnu kopiu clanku v aplikacii
            if(resumeData){
                [resumeData writeToFile:finalPath atomically:YES];
            }
            // preda cestu k vytvorenemu suboru
            destViewController.articleURL  = [NSURL fileURLWithPath:finalPath];
        }
        
        // ak je ulozeny clanok v aplikacii, tak sa vytvori cesta k suboru PDF priamo
        else{
            NSString *path2 = [[NSBundle mainBundle] pathForResource:articlePDF ofType:@"pdf"];
            destViewController.articleURL  = [NSURL fileURLWithPath:path2];
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"articleCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
   
    //nastavenie pozadia obrazovky na obrazok foto.jpg
    UIImage *foto = [UIImage imageNamed:@"foto.jpg" ];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:foto];
    imageView.contentMode = UIViewContentModeTopLeft;
    [tableView setBackgroundView:nil];
    tableView.backgroundView = imageView;
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    //postupne zobrazi nazov polozky v tabulke
    cell.textLabel.text  = [[articlesArray objectAtIndex:indexPath.row] objectForKey:@"article"];
 
    return cell;
}


@end
