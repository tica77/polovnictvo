//
//  SimpleEmailViewController.m
//  apka
//
//  Created by Luky on 02/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku KONTAKT


#import "SimpleEmailViewController.h"

@interface SimpleEmailViewController ()

@end

@implementation SimpleEmailViewController

@synthesize articleURL;

- (IBAction)showEmail:(id)sender {
    // Email Subject
    NSString *emailTitle = @"Ios aplikácia";
    // Email Content
    NSString *messageBody = @"<h3>Dobrý deň, </h3> kontaktujem Vás z dôvodu ";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"polovnictvo@lukyratica.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:YES];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
        default:
            break;
    }
    
    // zavrie rozhranie emailu
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
 }

    ///pre pridane prechody sa zobrazi fotokniha alebo diplomova praca vo formate PDF

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    SimpleEmailViewController *destViewController = segue.destinationViewController;

    if ([segue.identifier isEqualToString: @"fotokniha"]) {

        NSString *path = [[ NSBundle mainBundle] pathForResource:@"fotokniha" ofType:@"pdf"];
        NSURL *finalPath = [NSURL fileURLWithPath:path];
        destViewController.articleURL  = finalPath;
    }
    
    if ([segue.identifier isEqualToString: @"diplomka"]) {
        NSString *path = [[ NSBundle mainBundle] pathForResource:@"diplomka" ofType:@"pdf"];
        NSURL *finalPath = [NSURL fileURLWithPath:path];
        destViewController.articleURL  = finalPath;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
