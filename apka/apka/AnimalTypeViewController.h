//
//  AnimalTypeViewController.h
//  apka
//
//  Created by Luky on 20/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku ODSTREL

#import <UIKit/UIKit.h>

@interface AnimalTypeViewController: UITableViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *animalArray;
}

@property (strong, nonatomic) IBOutlet UITableView *animalTypeTable;

@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, strong) NSArray *nameItems;
@property (nonatomic, strong) NSMutableArray *tempArray;
@property (nonatomic, strong) NSMutableArray *srstnataArray;
@property (nonatomic, strong) NSMutableArray *pernataArray;
@property (nonatomic, strong) NSMutableArray *celorocneArray;
@property (nonatomic, strong) NSMutableArray *vynimkyArray;


@end
