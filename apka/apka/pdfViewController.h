//
//  pdfViewController.h
//  apka
//
//  Created by Luky on 13/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pdfViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *pdfView;


@end
