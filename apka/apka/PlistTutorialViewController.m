//
//  PlistTutorialViewController.m
//  PlistTutorial
//
//  Created by Nick Barrowclough on 6/5/13.
//  Copyright (c) 2013 iSoftware Developers. All rights reserved.
//

#import "PlistTutorialViewController.h"

@interface PlistTutorialViewController ()

@end

@implementation PlistTutorialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //create array from Plist document of all mountains
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory =  [paths objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"databaseOfHills.plist"];
    mountainsArray = [[[NSMutableArray alloc] initWithContentsOfFile:plistPath]mutableCopy];
    //NSLog(@"%@", mountainsArray);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logDictionary:(id)sender {
    [self refreshArray];
}

- (void) refreshArray {
    //create array from Plist document of all mountains
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory =  [paths objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"databaseOfHills.plist"];
    mountainsArray = [[[NSMutableArray alloc] initWithContentsOfFile:plistPath]mutableCopy];
    NSLog(@"%@", [mountainsArray objectAtIndex:0]);
}

- (IBAction)editDictionary:(id)sender {
    [self updatePlist];
}

//method to retrieve mountain object and update Plist
- (void) updatePlist {
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSDictionary *object in mountainsArray) {
        if ([[object objectForKey:@"Number"] isEqualToString:@"1"]) {
            [tempArray insertObject:object atIndex:0];
        }
    }
    NSMutableDictionary *dict = [tempArray objectAtIndex:0];
    [dict setObject:@"" forKey:@"Climbed"];
    [mountainsArray replaceObjectAtIndex:0 withObject:dict];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory =  [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"databaseOfHills.plist"];
    [mountainsArray writeToFile:path atomically:YES];
}


@end
