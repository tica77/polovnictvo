//
//  AnimalDetailViewController.m
//  apka
//
//  Created by Luky on 23/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import "AnimalDetailViewController.h"

@interface AnimalDetailViewController ()    {
    NSArray *tempArray; 
}

@end

@implementation AnimalDetailViewController
@synthesize animalAboutLabel;
@synthesize animalDateLabel;
@synthesize animalNameLabel;
@synthesize animalSrstnataLabel;
@synthesize animalName;
@synthesize animalAbout;
@synthesize animalDate;
@synthesize animalSrtstnata;
@synthesize animalURL;
@synthesize animalWeb;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


- (void)viewDidLoad
{
    // nastavenie informacii, ktore budu zobrazene na obrazovke
    animalNameLabel.text = animalName;
    animalAboutLabel.text = animalAbout;
    animalDateLabel.text = animalDate;
    if (animalSrtstnata) {
        animalSrstnataLabel.text = @"Patrí medzi srstnatú zver.";
    }
    else{
        animalSrstnataLabel.text = @"Patrí medzi pernatú zver.";
    }
    [super viewDidLoad];
    
    //nastavenie pozadia obrazovky na obrazok foto.jpg
    UIImage *foto = [UIImage imageNamed:@"foto.jpg" ];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:foto]];

}

    /// nastavi odkaz na adresu URL pre prechod na dalsiu obrazovku
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString: @"showAnimalWeb"]) {
        AnimalDetailViewController *destViewController = segue.destinationViewController;
        destViewController.animalWeb  =  animalURL;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return tdshe number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
