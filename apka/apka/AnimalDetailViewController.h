//
//  AnimalDetailViewController.h
//  apka
//
//  Created by Luky on 23/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku ODSTREL

#import <UIKit/UIKit.h>

@interface AnimalDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *animalNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *animalDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *animalAboutLabel;
@property (weak, nonatomic) IBOutlet UILabel *animalSrstnataLabel;

@property (nonatomic, strong) NSString *animalName;
@property (nonatomic, strong) NSString *animalSrtstnata;
@property (nonatomic, strong) NSString *animalURL;
@property (nonatomic, strong) NSString *animalAbout;
@property (nonatomic, strong) NSString *animalDate;
@property (nonatomic, strong) NSString *animalWeb;

@end