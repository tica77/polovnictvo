//
//  animalWebViewController.m
//  apka
//
//  Created by Luky on 23/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import "AnimalWebViewController.h"
#import "AnimalDetailViewController.h"
#import "Reachability.h"

@interface AnimalWebViewController (){
    
  Reachability *internetReachableFoo;
}

@end

@implementation AnimalWebViewController

@synthesize webView;
@synthesize animalWeb;
@synthesize loadingSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void) webViewDidStartLoad:(UIWebView *)webView {
    loadingSpinner.hidden = NO;
    [loadingSpinner startAnimating];
    
}

- (void)viewDidLoad
    {
        [super viewDidLoad];
        // Do any additional setup after loading the view
                internetReachableFoo = [Reachability reachabilityWithHostname:@"www.google.com"];
        __weak typeof(self) weakSelf = self;

        // Internet is reachable
        internetReachableFoo.reachableBlock = ^(Reachability*reach)     {
            NSURL *url = [NSURL URLWithString: [weakSelf.animalWeb stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            [weakSelf.webView loadRequest:requestObj];
        };
        
        // Internet is not reachable
        internetReachableFoo.unreachableBlock = ^(Reachability*reach)   {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Nemôžem sa pripojiť!"
                                                             message:@"Nemôžem načítať stránku pretože nemáte prístup na internet"
                                                            delegate:weakSelf
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
            [alert show];
        };
        
        [internetReachableFoo startNotifier];
       
}

- (void) webViewDidFinishLoad:(UIWebView *)webView  {
     [loadingSpinner stopAnimating];
    loadingSpinner.hidden = YES;

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
