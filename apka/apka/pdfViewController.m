//
//  pdfViewController.m
//  apka
//
//  Created by Luky on 13/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import "pdfViewController.h"

@interface pdfViewController ()

@end

@implementation pdfViewController



@synthesize pdfView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSString *urlAddress = @"/Users/luky/Desktop/iOS_app/GIT/apka/apka/zivotopis.pdf";
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [pdfView loadRequest:requestObj];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
