//
//  SimpleEmailViewController.h
//  apka
//
//  Created by Luky on 02/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku KONTAKT


#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface SimpleEmailViewController : UITableViewController <MFMailComposeViewControllerDelegate,  UITableViewDataSource,
    UITableViewDelegate> {}


- (IBAction)showEmail:(id)sender;

@property (nonatomic, strong) NSURL *articleURL;

@end
