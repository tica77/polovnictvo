//
//  AnimalTypeViewController.m
//  apka
//
//  Created by Luky on 20/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import "AnimalTypeViewController.h"
#import "SWRevealViewController.h"
#import "CellTableViewController.h"

@interface AnimalTypeViewController ()


@end


@implementation AnimalTypeViewController

@synthesize tempArray;
@synthesize srstnataArray;
@synthesize pernataArray;
@synthesize vynimkyArray;
@synthesize celorocneArray;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.animalTypeTable.dataSource = self;
    self.animalTypeTable.delegate = self;
 
    // zoznam statickych riadkov zoznamu
    _menuItems = @[@"one", @"second", @"three", @"four", @"five", @"six"];
    // zoznam nazvou pouzitych pre statiticke riadky
    _nameItems = @[@"Aktuálny odstrel", @"Srstnatá zver", @"Pernatá zver", @"Celoročný odstrel", @"Výnimky a zvernice", @"Všetká zver"];
    
    // nacitanie dat z AnimalInfo.plist
    NSString *plistPath;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:@"AnimalInfo.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle] pathForResource:@"AnimalInfo" ofType:@"plist"];
    }
    
    // vytvorenie pola s datamy z plistu
    animalArray = [[[NSMutableArray alloc] initWithContentsOfFile:plistPath]mutableCopy];
    
    //ziskanie datumu z aplikacie
    NSDate* date = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd"];
    
    // ulozenie aktualneho dna vo foramte NSInteger pre nasledne porovnavanie
    NSString* day = [formatter stringFromDate:date];
    NSInteger dayToday =[day integerValue];
    
    NSDateFormatter* formatterEEE = [[NSDateFormatter alloc] init];
    [formatterEEE setDateFormat:@"EEE"];
    NSString* nameDay = [formatterEEE stringFromDate:date];
    
    // ak pripada koniec lovu na sobotu alebo nedelu, je povoleny lov do konca vikendu
    if (([nameDay isEqualToString:@"Sun"]) ||([nameDay isEqualToString:@"Sat"] )) {
        dayToday--;
    }

    // ulozenie aktualneho mesiaca vo foramte NSInteger pre nasledne porovnavanie
    NSDateFormatter* formatterMM = [[NSDateFormatter alloc] init];
    [formatterMM setDateFormat:@"MM"];
    NSString* month = [formatterMM stringFromDate:date];
    NSInteger monthToday = [month integerValue];
    
    
    // vytvori pole aktualne povolenych zvierat na odstrel
    tempArray = [[[NSMutableArray alloc] init]mutableCopy];
    
    int number = 0;
    
    for (NSDictionary *object in animalArray) {
        // ulozenie udajov o zaciatku a konci obdobia povoleneho lovu zveri do formatov NSInteger
        NSInteger dayInN = [[object objectForKey:@"dayIn"] integerValue];
        NSInteger monthInN = [[object objectForKey:@"monthIn"] integerValue];
        NSInteger dayOutN = [[object objectForKey:@"dayOut"] integerValue];
        NSInteger monthOutN = [[object objectForKey:@"monthOut"] integerValue];
        
        // zistenie, ci aktualny datum patri do obdobia, kedy je povoleny lov zveri
        if ((monthOutN>=monthToday) || ((monthOutN<monthToday) && (monthOutN<=2)))  {
            if (monthToday >= 3) {
                if ((monthToday>monthInN)  ||
                    ((((monthToday==monthInN) && (dayToday>=dayInN))
                    || ((monthToday==monthOutN) && (dayToday<=dayOutN)))))     {
                    
                        // ak vyhovuje podmienkam, prida sa druh zveri do zoznamu tempArray
                        [tempArray insertObject:object atIndex:(number)];
                        number++;
                }
            }
            else  { //(monthToday<3)
                if (((monthToday<monthOutN) && (monthOutN <3) )
                    || ((monthToday==monthInN) && (dayToday>=dayInN))
                    || ((monthToday==monthOutN) && (dayToday<=dayOutN))) {
                    
                        // ak vyhovuje podmienkam, prida sa druh zveri do zoznamu tempArray
                        [tempArray insertObject:object atIndex:(number)];
                        number++;
                }
            }
        }
    }
    
    // vytvori pole srstnatych zvierat
    srstnataArray = [[[NSMutableArray alloc] init]mutableCopy];
    number = 0;
    for (NSDictionary *object in animalArray) {
        NSString *temp  = [object objectForKey:@"srstnata"];
        if ([temp isEqualToString: @"YES"]) {
            [srstnataArray insertObject:object atIndex:(number)];
            number++;
        }
    }
    
    // vytvori pole pre pernatu zver
    pernataArray = [[[NSMutableArray alloc] init]mutableCopy];
    number = 0;
    for (NSDictionary *object in animalArray) {
        NSString *temp  = [object objectForKey:@"srstnata"];
        if ([temp isEqualToString: @"NO"]) {
            [pernataArray insertObject:object atIndex:(number)];
            number++;
        }
    }

    // vytvori pole pre zver s celorocnym povolenim odstrelu
    celorocneArray = [[[NSMutableArray alloc] init]mutableCopy];
    number = 0;
    for (NSDictionary *object in animalArray) {
        NSString *temp  = [object objectForKey:@"about"];
        if ([temp isEqualToString: @"celoročne"]) {
            [celorocneArray insertObject:object atIndex:(number)];
            number++;
        }
    }
    
    // vytvori pole pre zver s vynimkami odstrelu
    vynimkyArray = [[[NSMutableArray alloc] init]mutableCopy];
    number = 0;
    for (NSDictionary *object in animalArray) {
        NSString *temp  = [object objectForKey:@"about"];
        if (!([temp isEqualToString: @"celoročne"] || [temp isEqualToString: @"--"])) {
            [vynimkyArray insertObject:object atIndex:(number)];
            number++;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // vrati pocet riadkov menuItems
    return self.menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [self.menuItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    //nastavenie pozadia obrazovky na obrazok foto.jpg
    UIImage *foto = [UIImage imageNamed:@"foto.jpg" ];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:foto];
    imageView.contentMode = UIViewContentModeTopLeft;
    [tableView setBackgroundView:nil];
    tableView.backgroundView = imageView;
    
    return cell;
}

    /// nastavi pre kazdu kategoriu zveri prislusny prechod, ktoremu preda zoznam druhov zvierat v kategorii
- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    if ([segue.identifier isEqualToString: @"showAktualne"]) {
        CellTableViewController *tableController = (CellTableViewController*) segue.destinationViewController;
        tableController.tempArray = tempArray;
    }
    
    if ([segue.identifier isEqualToString: @"showSrstnata"]) {
        CellTableViewController *tableController = (CellTableViewController*) segue.destinationViewController;
        tableController.tempArray = srstnataArray;
    }
    
    if ([segue.identifier isEqualToString: @"showPernata"]) {
        CellTableViewController *tableController = (CellTableViewController*) segue.destinationViewController;
        tableController.tempArray = pernataArray;
    }
    
    if ([segue.identifier isEqualToString: @"showCelorocne"]) {
        CellTableViewController *tableController = (CellTableViewController*) segue.destinationViewController;
        tableController.tempArray = celorocneArray;
    }
    
    if ([segue.identifier isEqualToString: @"showVynimka"]) {
        CellTableViewController *tableController = (CellTableViewController*) segue.destinationViewController;
        tableController.tempArray = vynimkyArray;
    }
    
    if ([segue.identifier isEqualToString: @"showCelkove"]) {
        CellTableViewController *tableController = (CellTableViewController*) segue.destinationViewController;
        tableController.tempArray = animalArray;
    }
    
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
    }

}

@end
