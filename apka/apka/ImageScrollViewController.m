//
//  ImageScrollViewController.m
//  ImageScroll
//
//  Created by Luky on 02/12/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import "ImageScrollViewController.h"

@interface ImageScrollViewController ()
@property (nonatomic, strong) UIImageView *imageView;

- (void)centerScrollViewContents;
- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer;

@end


@implementation ImageScrollViewController

@synthesize imagePath;

@synthesize scrollView;
@synthesize currrentIndex;
@synthesize PhotoArraySegue;
@synthesize imageView;

    /// vystredenie obrazku na stred obrazovky zadiadenia
- (void)centerScrollViewContents {
    CGSize boundsSize = self.scrollView.bounds.size;
    CGRect contentsFrame = self.imageView.frame;
    

    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f - 15;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = ((boundsSize.height - contentsFrame.size.height) / 2.0f) - 50;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    self.imageView.frame = contentsFrame;
}

    /// po rozpoznani dvojkliku na obrazovku dojde k presunu na nasledujuci obrazok
- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer {
    
    currrentIndex++;
    currrentIndex = (currrentIndex < 0) ? (9):
    currrentIndex % 10;
    
    [imageView removeFromSuperview];
    [self viewDidLoad];
    [self viewWillAppear:YES];

}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // nastavenie obrazku ktory chceme zobrazit, pripadne priblizit v scrollView
    NSFileManager *fileManager = [NSFileManager defaultManager];
    imagePath = [PhotoArraySegue objectAtIndex:currrentIndex];
    NSString *path2 = [[NSBundle mainBundle] pathForResource:imagePath ofType:@""];
    
    int i= path2.length;
    
    // ak obrazok existuje v aplikacii, dojde priamo k nacitaniu obrazku
    if (i > 4)
    {
        UIImage *image = [UIImage imageNamed:imagePath];
        self.imageView = [[UIImageView alloc] initWithImage:image];
        self.imageView.frame = (CGRect){.origin=CGPointMake(0.0f, 0.0f), .size=image.size};
        [self.scrollView addSubview:self.imageView];
    }
    
    // ak neexistuje, dojde k nacitaniu dat z cloudovej sluzby Parse.com, ulozenie do aplikacie a nasledne zobrazenie
    else{
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:imagePath];
        if ([fileManager fileExistsAtPath:path])
        {
            NSData *imgData = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path]];
            UIImage *image = [[UIImage alloc] initWithData:imgData];
            self.imageView = [[UIImageView alloc] initWithImage:image];
            self.imageView.frame = (CGRect){.origin=CGPointMake(0.0f, 0.0f), .size=image.size};
            [self.scrollView addSubview:self.imageView];
            
        }
        // v pripade neuplneho nacitania obrazku alebo inej chyby dojde k varovnej hlaske
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Nestiahnute data!"
                                                            message: @"Došlo k chybe pri sťahovaní dat."
                                                           delegate: self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    
    // nastavenie velkosti scroll view podla rozmerov obrazku
    self.scrollView.contentSize = self.imageView.image.size;
    
    // rozpoznavanie gest na dvojite kliknutie
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    [self.scrollView addGestureRecognizer:doubleTapRecognizer];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // nastavenie minimalnej a maximalnej hodnoty priblizenia obrazku
    CGRect scrollViewFrame = self.scrollView.frame;
    CGFloat scaleWidth = scrollViewFrame.size.width / self.scrollView.contentSize.width;
    CGFloat scaleHeight = scrollViewFrame.size.height / self.scrollView.contentSize.height;
    CGFloat minScale = MIN(scaleWidth, scaleHeight);
    
    self.scrollView.minimumZoomScale = minScale;
    self.scrollView.maximumZoomScale = 1.0f;
    self.scrollView.zoomScale = minScale;
    
    [self centerScrollViewContents];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    // vrati view ktore chceme zobrazit, konkretne image view
    
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // po priblizeni obrazku dojde k jeho vyzentrovaniu na stred obrazovky
    [self centerScrollViewContents];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    // po ukonceni chybovaj hlasky dojde k presunu na obrazovku o uroven naspat
    if ([buttonTitle isEqualToString:@"OK"]) {
        [self performSegueWithIdentifier:@"alertPush" sender:self];
    }
}


@end
