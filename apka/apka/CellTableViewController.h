//
//  CellTableViewController.h
//  apka
//
//  Created by Luky on 31/10/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku ODSTREL

#import <UIKit/UIKit.h>
#import <Parse/Parse.h> 

@interface CellTableViewController: UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@property (nonatomic, strong) NSArray *tempArray;


@end
