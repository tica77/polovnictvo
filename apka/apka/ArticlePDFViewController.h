//
//  ArticlePDFViewController.h
//  apka
//
//  Created by Luky on 24/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku CLANKY

#import <UIKit/UIKit.h>

@interface ArticlePDFViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic, strong) NSURL *articleURL;
@end


