//
//  photoTableViewController.m
//  apka
//
//  Created by Luky on 02/12/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import "PhotoTableViewController.h"
#import "Reachability.h"

@interface PhotoTableViewController (){
   
    Reachability *internetReachableFoo;
    
}

@end

@implementation PhotoTableViewController

@synthesize photoTableView;
@synthesize photoSegue;
@synthesize photoArray;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
    self.tabBarController.tabBar.hidden = NO;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBarController.tabBar.hidden = NO;
    
    // nacita zoznam albumov ulozenych v aplikacii zo suboru photoArray.plist
    NSString *plistPath;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:@"photoArray.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle] pathForResource:@"photoArray" ofType:@"plist"];
    }
    // vytvori pole photoArray do ktoreho nahra udaje z photoArray.plist
    photoArray = [[[NSMutableArray alloc] initWithContentsOfFile:plistPath]mutableCopy];

    [photoTableView reloadData];
    
    // kontrola ci je dostupne internetove pripojenie
    internetReachableFoo = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // ak je internet dostupny...
    __weak typeof(self) weakSelf = self;
    internetReachableFoo.reachableBlock = ^(Reachability*reach)
    {
       
    // ...potom aktualizuje zoznam fotoalbumov z triedy fotoAlbums z Parse.com
    dispatch_async(dispatch_get_main_queue(), ^{
            PFQuery *retrievePhotos = [PFQuery queryWithClassName:@"fotoAlbums"];
            [retrievePhotos findObjectsInBackgroundWithBlock:^(NSArray *object, NSError *error) {
                [weakSelf.photoArray addObjectsFromArray:object];
                [weakSelf.photoTableView reloadData];
            }];
        });
        
    
    };
    [internetReachableFoo startNotifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // vrati pocet riadkov zhodnych z dlzkou pola photoArray
    return photoArray.count;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // pre kazdy riadok je vytvoreny prechod na zobrazenie nahladou fotografii celeho albumu na novej obrazovke
    if ([segue.identifier isEqualToString: @"showCollectionPhoto"]) {
        NSIndexPath *indexPath = [self.photoTableView indexPathForSelectedRow];
        PhotoTableViewController *destViewController = segue.destinationViewController;
        
        photoSegue = [[photoArray objectAtIndex:indexPath.row] objectForKey:@"imageName"];
        // tento prechod obsahuje n8zov fotoalbumy, z ktoreho ma nahaldy nacitat
        destViewController.photoSegue = photoSegue;
        
           }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    //nastavenie pozadia obrazovky na obrazok foto.jpg
    UIImage *foto = [UIImage imageNamed:@"foto.jpg" ];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:foto];
    imageView.contentMode = UIViewContentModeTopLeft;
    [tableView setBackgroundView:nil];
    tableView.backgroundView = imageView;
    self.tabBarController.tabBar.hidden = NO;

    if (cell == nil) {
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
    }
    // jednotlivym riadkom nastavi nazov na meno fotoAlbumu
    cell.textLabel.text  = [[photoArray objectAtIndex:indexPath.row] objectForKey:@"title"];
    return cell;
}


@end


