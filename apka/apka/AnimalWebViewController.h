//
//  AnimalWebViewController.h
//  apka
//
//  Created by Luky on 23/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//
// pre zalozku ODSTREL

#import <UIKit/UIKit.h>

@interface AnimalWebViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;

@property (nonatomic, strong) NSString *animalWeb;

@end
