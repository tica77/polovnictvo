//
//  SWRevealCell.h
//  apka
//
//  Created by Luky on 20/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//


#import <UIKit/UIKit.h>

/// pre zobrazenie zoznamu kategorii pre vyber v sekcii Odstrel, pouzite v AnimalTypeViewController
@interface SWRevealCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleCellLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageCellLabel;

@end
