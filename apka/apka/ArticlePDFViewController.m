//
//  ArticlePDFViewController.m
//  apka
//
//  Created by Luky on 24/11/13.
//  Copyright (c) 2013 Luky. All rights reserved.
//

#import "ArticlePDFViewController.h"

@interface ArticlePDFViewController ()

@end

@implementation ArticlePDFViewController


@synthesize webView;
@synthesize articleURL;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


- (void)viewDidLoad
{
    // nastavi cestu prevziatu z articleURL pre zobrazenie pdf suboru s obsahom clanku
    [super viewDidLoad];
    NSURLRequest *request = [NSURLRequest requestWithURL:articleURL];
    [webView loadRequest:request];
    [webView setScalesPageToFit:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
